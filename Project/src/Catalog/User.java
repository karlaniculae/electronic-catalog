package Tema;

public abstract class User {
    private String firstName, lastName;

    public User(String lastName, String firstName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String toString() {
        return lastName + " " + firstName;
    }

    public String getName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
}
