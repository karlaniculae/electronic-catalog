package Tema;

public class Grade implements Comparable<Grade>, Cloneable {
    private Double partialScore, examScore;
    private Student student;
    private String course;

    public Grade(Student student, String course) {
        this.student = student;
        this.course = course;
    }

    public Double getPartialScore() {
        return partialScore;
    }

    public void setPartialScore(Double partialScore) {
        this.partialScore = partialScore;
    }

    public Double getExamScore() {
        return examScore;
    }

    public void setExamScore(Double examScore) {
        this.examScore = examScore;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Double getTotal() {
        return partialScore + examScore;
    }

    public int compareTo(Grade other) {
        return getTotal().compareTo(other.getTotal());
    }

    public Grade clone() {
        try {
            return (Grade) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public String toString() {
        return "Nota:" + student + "\n" + "Curs:" + course + "\n" + "Nota partiala:" + getPartialScore() + "\n" + "Nota examen:" + getExamScore() + "\n" + "Total nota curs" + " " + getTotal() + "\n";
    }
}
