package Tema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScoreVisitor implements Visitor {
    HashMap<Teacher, ArrayList<Tuple<Student, String, Double>>> examScores = new HashMap<>();
    HashMap<Assistant, ArrayList<Tuple<Student, String, Double>>> partialScores = new HashMap<>();

    public HashMap<Teacher, ArrayList<Tuple<Student, String, Double>>> getExamScores() {
        return examScores;
    }

    public HashMap<Assistant, ArrayList<Tuple<Student, String, Double>>> getPartialScores() {
        return partialScores;
    }

    public void addMap(Teacher teacher, Student student, String studentCourse, double grade) {
        ArrayList<Tuple<Student, String, Double>> s;
        if (examScores.containsKey(teacher)) {
            s = examScores.get(teacher);
        } else {
            s = new ArrayList<>();
            examScores.put(teacher, s);
        }
        Tuple<Student, String, Double> t = new Tuple<>(student, studentCourse, grade);
        s.add(t);
    }

    public void addMap2(Assistant assistant, Student student, String studentCourse, Double grade) {
        ArrayList<Tuple<Student, String, Double>> s;
        if (partialScores.containsKey(assistant)) {
            s = partialScores.get(assistant);
        } else {
            s = new ArrayList<>();
            partialScores.put(assistant, s);
        }
        Tuple<Student, String, Double> t = new Tuple<>(student, studentCourse, grade);
        s.add(t);
    }

    public void visit(Assistant assistant) {
        ArrayList<Tuple<Student, String, Double>> list = null;
        for (Assistant t : partialScores.keySet()) {
            if (t.equals(assistant)) {
                list = partialScores.get(t);
                break;
            }
        }
        if (list == null) {
            System.out.println("List is empty");
            return;
        }
        Catalog cat = Catalog.getInstance();
        List<Course> courses = cat.getCourses();
        for (Tuple<Student, String, Double> tup : list) {
            Student s = tup.getStr();
            String c = tup.getCourse();
            Double n = tup.getNota();
            for (Course course : courses) {
                if (course.getName().equals(c)) {
                    Grade gr = course.getGrade(s);
                    if (gr == null) {
                        gr = new Grade(s, c);
                        gr.setPartialScore(n);
                        gr.setExamScore(0.0);
                        course.addGrade(gr);
                        cat.notifyObservers(gr);
                    } else {
                        gr.setPartialScore(n);
                        cat.notifyObservers(gr);
                    }
                }
            }
        }
    }

    public void visit(Teacher teacher) {
        ArrayList<Tuple<Student, String, Double>> list = null;
        for (Teacher t : examScores.keySet()) {
            if (t.equals(teacher)) {
                list = examScores.get(t);
                break;
            }
        }
        if (list == null) {
            System.out.println("List is empty");
            return;
        }
        Catalog cat = Catalog.getInstance();
        List<Course> courses = cat.getCourses();
        for (Tuple<Student, String, Double> tup : list) {
            Student s = tup.getStr();
            String c = tup.getCourse();
            Double n = tup.getNota();
            for (Course course : courses) {
                if (course.getName().equals(c)) {
                    Grade gr = course.getGrade(s);
                    if (gr == null) {
                        gr = new Grade(s, c);
                        gr.setExamScore(n);
                        gr.setPartialScore(0.0);
                        course.addGrade(gr);
                        cat.notifyObservers(gr);
                    } else {
                        gr.setExamScore(n);
                        cat.notifyObservers(gr);
                    }
                }
            }
        }
    }

    class Tuple<T1, T2, T3> {
        private T1 student;
        private T2 course;
        private T3 nota;

        public Tuple(T1 student, T2 course, T3 nota) {
            this.student = student;
            this.nota = nota;
            this.course = course;
        }

        public T1 getStr() {
            return student;
        }

        public T3 getNota() {
            return nota;
        }

        public T2 getCourse() {
            return course;
        }

        public String toString() {
            return student + " " + nota + " " + course;
        }
    }
}

