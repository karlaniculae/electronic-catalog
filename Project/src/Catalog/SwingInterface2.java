package Tema;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SwingInterface2 extends JFrame implements ActionListener, ListSelectionListener {
    static ScoreVisitor visit = new ScoreVisitor();
    JButton button;
    JButton button1;
    JList<String> list;
    DefaultListModel<String> model;
    JLabel label1;
    JButton button2;
    JTextArea information;
    JTextField searchName;
    JScrollPane first;
    int ok12 = -1;

    public SwingInterface2() {
        super("Catalog");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel cardPanel = new JPanel(new CardLayout());

        JPanel first1 = new JPanel();
        JPanel first2 = new JPanel();
        first1.setLayout(new GridLayout(3, 3));
        first1.setLayout(new GridLayout(2, 1));
        label1 = new JLabel("Search teacher/assistant:");
        label1.setForeground(Color.orange);
        information = new JTextArea();
        information.setEditable(false);
        searchName = new JTextField();
        button = new JButton("Assistant");
        button.addActionListener(this);
        button.setForeground(Color.CYAN);
        button1 = new JButton("Validation");
        button1.setForeground(Color.RED);
        button1.addActionListener(this);
        button.setForeground(Color.CYAN);
        button2 = new JButton("Teacher");
        button2.addActionListener(this);
        button2.setForeground(Color.CYAN);
        list = new JList<>();
        list.addListSelectionListener(this);
        first = new JScrollPane(list);
        first.setVisible(true);
        model = new DefaultListModel<String>();
        list.setModel(model);
        first1.add(label1);
        first1.add(searchName);
        first1.add(button1);
        first1.add(button);
        first1.add(button2);
        add(first1, BorderLayout.NORTH);
        add(information, BorderLayout.CENTER);
        add(first, BorderLayout.SOUTH);
        pack();
        setSize(500, 500);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String firstname = null;
        String lastName = null;
        if (e.getSource() instanceof JButton) {
            model.clear();
            Catalog c = Catalog.getInstance();
            JButton button4 = (JButton) e.getSource();
            if (button4.getText().equals(("Teacher"))) {
                ok12 = 1;
                model.clear();
                String[] s = searchName.getText().split(" ");
                int ok = 0;
                if (s.length > 1) {
                    firstname = s[1];
                    lastName = s[0];
                    for (Course courses : c.getCourses()) {
                        if (courses.getProfesor().getLastName().equals(lastName) && courses.getProfesor().getName().equals(firstname)) {
                            model.addElement(courses.getName());
                            ok = 1;
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "You didn't write the complete name!", "Warning", JOptionPane.WARNING_MESSAGE);
                    searchName.setText("");
                }
                if (ok == 0) {
                    JOptionPane.showMessageDialog(null, "The TEACHER does not exists!", "Warning", JOptionPane.WARNING_MESSAGE);
                    searchName.setText("");
                }
            } else if (button4.getText().equals(("Assistant"))) {
                ok12 = 0;
                model.clear();
                String[] s = searchName.getText().split(" ");
                int ok = 0;
                if (s.length > 1) {
                    firstname = s[1];
                    lastName = s[0];
                    for (Course courses : c.getCourses()) {
                        for (Assistant ass : courses.getAsistenti()) {
                            if (ass.getLastName().equals(lastName) && ass.getName().equals(firstname)) {
                                model.addElement(courses.getName());
                                ok = 1;
                            }
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "You didn't write the complete name", "Warning", JOptionPane.WARNING_MESSAGE);
                    searchName.setText("");
                }
                if (ok == 0) {
                    JOptionPane.showMessageDialog(null, "The ASSISTANT does not exists", "Warning", JOptionPane.WARNING_MESSAGE);
                    searchName.setText("");
                }
            } else if (button4.getText().equals("Validation")) {
                int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to validate the grades?", "Confirmation", JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    if (ok12 == 1) {
                        String[] s = searchName.getText().split(" ");
                        firstname = s[1];
                        lastName = s[0];
                        ArrayList<Course> n = new ArrayList<>();
                        JTextArea textArea = new JTextArea();

                        for (Course courses : c.getCourses()) {
                            if (courses.getProfesor().getLastName().equals(lastName) && courses.getProfesor().getName().equals(firstname)) {
                                courses.getProfesor().accept(visit);
                                n.add(courses);
                            }
                        }
                        for (int i = 0; i < n.size(); i++) {
                            for (Grade g : n.get(i).getGrades())
                                textArea.append(g.toString());
                        }
                        JScrollPane scrollPane = new JScrollPane(textArea);
                        JOptionPane.showMessageDialog(null, scrollPane, "Grades have been successfully added!", JOptionPane.INFORMATION_MESSAGE);

                    } else if (ok12 == 0) {
                        String[] s = searchName.getText().split(" ");
                        firstname = s[1];
                        lastName = s[0];
                        ArrayList<Course> n = new ArrayList<>();
                        JTextArea textArea = new JTextArea();

                        for (Course courses : c.getCourses()) {
                            for (Assistant nume : courses.getAsistenti()) {
                                if (nume.getLastName().equals(lastName) && nume.getName().equals(firstname)) {
                                    nume.accept(visit);
                                    n.add(courses);
                                }
                            }
                        }
                        for (int i = 0; i < n.size(); i++) {
                            for (Grade g : n.get(i).getGrades())
                                textArea.append(g.toString());
                        }
                        JScrollPane scrollPane = new JScrollPane(textArea);
                        JOptionPane.showMessageDialog(null, scrollPane, "Grades have been successfully added!", JOptionPane.INFORMATION_MESSAGE);

                    }
                }
            } else {
            }
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        int index;
        if (list.isSelectionEmpty())
            return;
        else {
            if (ok12 == 1) {
                information.setText("");
                Course b = null;
                index = list.getSelectedIndex();
                String str = model.get(index);
                Catalog catalog = Catalog.getInstance();
                for (Course curs : catalog.getCourses()) {
                    if (curs.getName().equals(str))
                        b = curs;
                }
                String[] s = searchName.getText().split(" ");
                String firstname = s[1];
                String lastName = s[0];
                Teacher teacher = null;
                for (Course curs : catalog.getCourses()) {
                    if (curs.getProfesor().getLastName().equals(lastName) && curs.getProfesor().getName().equals(firstname))
                        teacher = curs.getProfesor();
                }
                information.setBorder(BorderFactory.createTitledBorder("Validation grades for the course:" + b.getName()));
                for (Map.Entry<Teacher, ArrayList<ScoreVisitor.Tuple<Student, String, Double>>> entry : visit.getExamScores().entrySet()) {
                    Teacher key = entry.getKey();
                    ArrayList<ScoreVisitor.Tuple<Student, String, Double>> value = entry.getValue();
                    if (key.getLastName().equals(teacher.getLastName()) && key.getName().equals(teacher.getName())) {
                        for (ScoreVisitor.Tuple<Student, String, Double> score : value) {
                            Student student = score.getStr();
                            String c = score.getCourse();
                            Double g = score.getNota();
                            information.append(student + ":" + g + "\n");
                        }
                    }
                }

            } else if (ok12 == 0) {
                information.setText("");
                Course b = null;
                index = list.getSelectedIndex();
                String str = model.get(index);
                Catalog catalog = Catalog.getInstance();
                for (Course curs : catalog.getCourses()) {
                    if (curs.getName().equals(str))
                        b = curs;
                }
                String[] s = searchName.getText().split(" ");
                String firstname = s[1];
                String lastName = s[0];
                Assistant ass = null;
                for (Course curs : catalog.getCourses()) {
                    for (Assistant st : curs.getAsistenti())
                        if (st.getLastName().equals(lastName) && st.getName().equals(firstname))
                            ass = st;
                }
                information.setBorder(BorderFactory.createTitledBorder("Validation grades for the course:" + b.getName()));
                for (Map.Entry<Assistant, ArrayList<ScoreVisitor.Tuple<Student, String, Double>>> entry : visit.getPartialScores().entrySet()) {
                    Assistant key = entry.getKey();
                    ArrayList<ScoreVisitor.Tuple<Student, String, Double>> value = entry.getValue();
                    if (key.getLastName().equals(ass.getLastName()) && key.getName().equals(ass.getName())) {
                        for (ScoreVisitor.Tuple<Student, String, Double> score : value) {
                            Student student = score.getStr();
                            String c = score.getCourse();
                            Double g = score.getNota();
                            information.append(student + ":" + g + "\n");
                        }
                    }
                }

            }
        }

    }

    public static void main(String args[]) {
        SwingInterface2 swing = new SwingInterface2();

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("/Users/karlaniculae/IdeaProjects/Tema/src/Tema/Tester1.json"));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray courses = (JSONArray) jsonObject.get("courses");
            Catalog catalog = Catalog.getInstance();
            for (int i = 0; i < courses.size(); i++) {
                JSONObject course = (JSONObject) courses.get(i);
                Course curs = null;


                String strategy = (String) course.get("strategy");
                Strategy strategy1 = null;
                if (strategy.equals("BestExamScore")) {
                    strategy1 = new BestExamScore();
                } else if (strategy.equals("BestPartialScore")) {
                    strategy1 = new BestPartialScore();
                } else if (strategy.equals("BestTotalScore")) {
                    strategy1 = new BestTotalScore();
                }
                String name = (String) course.get("name");
                ArrayList<Teacher> teachers = new ArrayList<>();
                JSONObject teacher = (JSONObject) course.get("teacher");
                String firstName = (String) teacher.get("firstName");
                String lastName = (String) teacher.get("lastName");
                Teacher teacher1 = (Teacher) UserFactory.getUser(UserFactory.UserType.Teacher, lastName, firstName);

                teachers.add(teacher1);
                JSONArray assistants = (JSONArray) course.get("assistants");
                HashSet<Assistant> asistenti = new HashSet<Assistant>();
                if (assistants != null && !assistants.isEmpty()) {
                    for (int j = 0; j < assistants.size(); j++) {
                        JSONObject assistant = (JSONObject) assistants.get(j);
                        if (assistant != null) {

                            String nume = (String) assistant.get("firstName");
                            String prenume = (String) assistant.get("lastName");
                            Assistant a = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, nume, prenume);
                            asistenti.add(a);
                        }
                    }
                }
                if (course.get("type").equals("FullCourse")) {
                    curs = new FullCourse.FullCourseBuilder(name).asistenti(asistenti).profesor(teacher1).strategy(strategy1).build();
                } else {
                    curs = new PartialCourse.PartialCourseBuilder(name).asistenti(asistenti).profesor(teacher1).strategy(strategy1).build();
                }

                JSONArray groupsArray = (JSONArray) course.get("groups");
                for (Object groupObject : groupsArray) {
                    JSONObject group = (JSONObject) groupObject;
                    String ID = (String) group.get("ID");
                    JSONObject assistant = (JSONObject) group.get("assistant");
                    String assistantFirstName = (String) assistant.get("firstName");
                    String assistantLastName = (String) assistant.get("lastName");
                    Assistant ass1 = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, assistantLastName, assistantFirstName);
                    curs.addAssistant(ID, ass1);
                    Group g = new Group(ID, ass1);
                    JSONArray studentsArray = (JSONArray) group.get("students");
                    for (Object studentObject : studentsArray) {
                        JSONObject student = (JSONObject) studentObject;
                        String studentFirstName = (String) student.get("firstName");
                        String studentLastName = (String) student.get("lastName");
                        Student s = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                        JSONObject mother = (JSONObject) student.get("mother");
                        Parent p1 = null;
                        Parent p2 = null;
                        if (mother != null) {
                            String motherFirstName = (String) mother.get("firstName");
                            String motherLastName = (String) mother.get("lastName");
                            p1 = (Parent) UserFactory.getUser(UserFactory.UserType.Parent, motherLastName, motherFirstName);
                            boolean isObserver = false;
                            for (Observer obs : catalog.getObs()) {
                                if (obs != null && obs instanceof Parent) {
                                    Parent parent = (Parent) obs;
                                    if (parent.getName().equals(p1.getName()) && parent.getLastName().equals(p1.getLastName())) {
                                        isObserver = true;
                                        break;
                                    }
                                }
                            }
                            if (!isObserver) {
                                catalog.addObserver(p1);
                            }
                        }
                        JSONObject father = (JSONObject) student.get("father");
                        if (father != null) {
                            String fatherFirstName = (String) father.get("firstName");
                            String fatherLastName = (String) father.get("lastName");
                            p2 = (Parent) UserFactory.getUser(UserFactory.UserType.Parent, fatherLastName, fatherFirstName);
                            boolean isObserver = false;
                            for (Observer obs : catalog.getObs()) {
                                if (obs != null && obs instanceof Parent) {
                                    Parent parent = (Parent) obs;
                                    if (parent.getName().equals(p2.getName()) && parent.getLastName().equals(p2.getLastName())) {
                                        isObserver = true;
                                        break;
                                    }
                                }
                            }
                            if (!isObserver) {
                                catalog.addObserver(p2);
                            }
                        }
                        s.setMother(p1);
                        s.setFather(p2);
                        g.add(s);

                    }
                    curs.addGroup(g);
                }
                catalog.addCourse(curs);
            }
            Grade g = null;
            JSONArray examScores = (JSONArray) jsonObject.get("examScores");

            for (int j = 0; j < examScores.size(); j++) {
                JSONObject examScore = (JSONObject) examScores.get(j);
                Teacher teacher3 = null;
                Student s2 = null;
                JSONObject teacher = (JSONObject) examScore.get("teacher");

                if (teacher != null) {
                    String teacherFirstName = (String) teacher.get("firstName");
                    String teacherLastName = (String) teacher.get("lastName");
                    teacher3 = (Teacher) UserFactory.getUser(UserFactory.UserType.Teacher, teacherLastName, teacherFirstName);

                }
                JSONObject student = (JSONObject) examScore.get("student");
                if (student != null) {
                    String studentFirstName = (String) student.get("firstName");
                    String studentLastName = (String) student.get("lastName");
                    s2 = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                }
                String course = (String) examScore.get("course");
                double grade = (double) examScore.get("grade");
                for (Course coursess : catalog.getCourses()) {
                    if (coursess.getName().equals(course)) {
                        for (Student std : coursess.getAllStudents()) {
                            if (s2.getLastName().equals(std.getLastName()) && s2.getName().equals(std.getName())) {
                                visit.addMap(teacher3, std, course, grade);
                            }
                        }
                    }

                }

            }
            JSONArray partialScores = (JSONArray) jsonObject.get("partialScores");
            for (int j = 0; j < partialScores.size(); j++) {
                JSONObject partialScore = (JSONObject) partialScores.get(j);
                Assistant assistant3 = null;
                Student s2 = null;

                JSONObject assistant = (JSONObject) partialScore.get("assistant");
                if (assistant != null) {
                    String assistantFirstName = (String) assistant.get("firstName");
                    String assistantLastName = (String) assistant.get("lastName");
                    assistant3 = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, assistantLastName, assistantFirstName);
                }

                JSONObject student = (JSONObject) partialScore.get("student");
                if (student != null) {
                    String studentFirstName = (String) student.get("firstName");
                    String studentLastName = (String) student.get("lastName");
                    s2 = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                }
                String course = (String) partialScore.get("course");
                double grade = (double) partialScore.get("grade");
                for (Course coursess : catalog.getCourses()) {
                    if (coursess.getName().equals(course)) {
                        for (Student std : coursess.getAllStudents()) {
                            if (s2.getLastName().equals(std.getLastName()) && s2.getName().equals(std.getName())) {
                                visit.addMap2(assistant3, std, course, grade);
                            }
                        }
                    }

                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
