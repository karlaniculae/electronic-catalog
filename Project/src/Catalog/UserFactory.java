package Tema;

public class UserFactory {
    public enum UserType {
        Parent,
        Student,
        Assistant,
        Teacher
    }

    public static User getUser(UserType userType, String lastName, String firstName) {
        switch (userType) {
            case Parent:
                return new Parent(lastName, firstName);
            case Student:
                return new Student(lastName, firstName);
            case Assistant:
                return new Assistant(lastName, firstName);
            case Teacher:
                return new Teacher(lastName, firstName);

        }
        throw new IllegalArgumentException("The user type" + userType + "is not recognized.");
    }
}
