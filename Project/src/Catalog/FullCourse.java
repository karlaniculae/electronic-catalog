package Tema;

import java.util.ArrayList;
import java.util.HashMap;

public class FullCourse extends Course {

    public FullCourse(CourseBuilder builder) {
        super(builder);
    }

    public ArrayList<Student> getGraduatedStudents() {
        ArrayList<Student> list = new ArrayList<Student>();
        double partial = 0;
        double exam = 0;
        for (int i = 0; i < getAllStudents().size(); i++) {
            Student s = getAllStudents().get(i);
            HashMap<Student, Grade> studentGrades = gettAllStudentGrades();
            if (studentGrades.containsKey(s)) {
                partial = studentGrades.get(s).getPartialScore();
                exam = studentGrades.get(s).getExamScore();
            }
            if (partial >= 3 && exam >= 2) {
                list.add(s);
            }
        }
        return list;
    }

    public static class FullCourseBuilder extends CourseBuilder {
        public FullCourseBuilder(String name) {
            super(name);
        }

        @Override
        public Course build() {
            return new FullCourse(this);
        }
    }
}
