package Tema;

import java.util.ArrayList;
import java.util.HashMap;

public interface Strategy {
    Student select(HashMap<Student, Grade> students);
}
