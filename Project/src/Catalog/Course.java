package Tema;

import java.util.*;
import java.util.HashMap;

public abstract class Course {
    private Strategy strategy;
    private String name;
    private Teacher profesor;
    private HashSet<Assistant> asistenti;
    private ArrayList<Grade> grades;
    private HashMap<String, Group> grupe;
    private int credite;

    private Snapshot snapshot;


    public Course(CourseBuilder builder) {
        this.name = builder.name;
        this.profesor = builder.profesor;
        this.asistenti = new HashSet<Assistant>();
        this.grades = new ArrayList<Grade>();
        this.grupe = new HashMap<String, Group>();
        this.credite = builder.credite;
        this.strategy = builder.strategy;
    }

    public String getName() {
        return name;
    }

    public Teacher getProfesor() {
        return profesor;
    }

    public HashSet<Assistant> getAsistenti() {
        return asistenti;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public HashMap<String, Group> getGrupe() {
        return grupe;
    }

    public int getCredite() {
        return credite;
    }

    public String toString() {
        return "Course:" + this.name + " " + this.profesor + " " + this.asistenti + " " + this.grades + " " + this.grupe + " " + this.credite;
    }

    public void addAssistant(String ID, Assistant assistant) {
        Group g = grupe.get(ID);
        if (g != null) {
            g.setAssistant(assistant);
        }
        if (!asistenti.contains(assistant))
            asistenti.add(assistant);
    }

    public void addStudent(String ID, Student student) {
        Group g = grupe.get(ID);
        g.add(student);
    }

    public void addGroup(Group group) {
        grupe.put(group.getID(), group);
    }

    public void setGroup(HashMap<String, Group> grupe) {
        this.grupe = grupe;
    }

    public void addGroup(String ID, Assistant assistant) {
        Group group = new Group(ID, assistant);
        grupe.put(group.getID(), group);
    }

    public void addGroup(String ID, Assistant assist, Comparator<Student> comp) {
        Group group = new Group(ID, assist, comp);
        grupe.put(group.getID(), group);
    }

    public Grade getGrade(Student student) {
        for (int i = 0; i < grades.size(); i++) {
            if (grades.get(i).getStudent().equals(student)) {
                return grades.get(i);
            }
        }
        return null;
    }

    public void addGrade(Grade grade) {
        grades.add(grade);
    }

    public ArrayList<Student> getAllStudents() {
        ArrayList<Student> students = new ArrayList<>();
        for (Group g : grupe.values())
            for (Student student : g) {
                students.add(student);
            }
        return students;
    }

    public HashMap<Student, Grade> gettAllStudentGrades() {
        HashMap<Student, Grade> map = new HashMap<Student, Grade>();
        for (Grade grade : grades) {
            map.put(grade.getStudent(), grade);
        }
        return map;
    }

    public Student getBestStudent() {
        return strategy.select(gettAllStudentGrades());
    }

    private class Snapshot {
        private ArrayList<Grade> gradesBackup;

        public Snapshot(ArrayList<Grade> grades) {
            this.gradesBackup = new ArrayList<>(grades);
        }
    }

    public void makeBackup() {
        ArrayList<Grade> g = new ArrayList<>();
        for (Grade gr : grades) {
            g.add(gr.clone());
        }
        snapshot = new Snapshot(g);
    }

    public void undo() {
        this.grades = snapshot.gradesBackup;
        for (Grade g : grades)
            System.out.println(g + "\n");
    }

    public abstract ArrayList<Student> getGraduatedStudents();

    public static abstract class CourseBuilder {
        private Strategy strategy;
        private final String name;
        private Teacher profesor;
        private HashSet<Assistant> asistenti;
        private ArrayList<Grade> grades;
        private HashMap<String, Group> grupe;
        private int credite;

        public CourseBuilder(String name) {
            this.name = name;
        }

        public CourseBuilder profesor(Teacher profesor) {
            this.profesor = profesor;
            return this;
        }

        public CourseBuilder credite(Teacher profesor) {
            this.credite = credite;
            return this;
        }

        public CourseBuilder asistenti(HashSet<Assistant> asistenti) {
            this.asistenti = asistenti;
            return this;
        }

        public CourseBuilder grades(ArrayList<Grade> grades) {
            this.grades = grades;
            return this;
        }

        public CourseBuilder grupe(HashMap<String, Group> grupe) {
            this.grupe = grupe;
            return this;
        }

        public CourseBuilder strategy(Strategy strategy) {
            this.strategy = strategy;
            return this;
        }

        public abstract Course build();
    }
}
