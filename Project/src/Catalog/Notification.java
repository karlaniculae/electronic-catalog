package Tema;

import java.util.Vector;

public class Notification {
    private boolean notification = false;
    private Double oldgrade;
    private Double examoldgrade;
    private Double partialoldgrade;
    private Grade grade;

    Notification(Grade grade) {
        this.grade = grade;
    }

    public String toString() {
        return grade.getStudent().getLastName() + " " + grade.getStudent().getName() + " reaceived at course" + " " + grade.getCourse()+":" + "\n" + "Partial Score:" + grade.getPartialScore() + "\n" + "Exam Score:" + grade.getExamScore() + "\n" + "Course Total Grade:" + " " + grade.getTotal() + "\n";
    }
}



