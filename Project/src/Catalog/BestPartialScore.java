package Tema;

import java.util.HashMap;

public class BestPartialScore implements Strategy {
    public Student select(HashMap<Student, Grade> students) {
        double examnote = -1;
        Student s = null;
        for (HashMap.Entry<Student, Grade> entry : students.entrySet()) {
            if (examnote < entry.getValue().getPartialScore())
                s = entry.getKey();
        }
        return s;
    }
}

