package Tema;

public class Parent extends User implements Observer {
    public Parent(String lastName, String firstName) {
        super(lastName, firstName);
    }

    public void update(Notification notification) {
        System.out.println(notification);
    }
}
