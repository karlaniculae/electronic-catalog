package Tema;

import java.util.Comparator;
import java.util.TreeSet;

public class Group extends TreeSet<Student> {
    private String ID;
    private Assistant assistant;
    private Comparator<Student> comp;

    public Group(String ID, Assistant assistant, Comparator<Student> comp) {
        super(comp);
        this.ID = ID;
        this.assistant = assistant;
        this.comp = comp;
    }

    public Group(String ID, Assistant assistant) {
        this(ID, assistant, null);
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public TreeSet<Student> getStudents() {
        return this;
    }

    public String getID() {
        return ID;
    }

    public void setAssistant(Assistant assistant) {
        this.assistant = assistant;
    }

    public Assistant getAssistent() {
        return assistant;
    }

    public void setComp(Comparator<Student> comp) {
        this.comp = comp;
    }

    public Comparator<Student> getComp() {
        return comp;
    }

    public int hashCode() {
        return ID.hashCode();
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Group)) return false;
        Group other = (Group) o;
        return ID.equals(other.ID);
    }

    public String toString() {
        return "ID Grup:" + ID + "\n" + "Assistentul grupei:" + assistant + " ";
    }
}
