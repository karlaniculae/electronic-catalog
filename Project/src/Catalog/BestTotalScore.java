package Tema;

import java.util.HashMap;

public class BestTotalScore implements Strategy {
    public Student select(HashMap<Student, Grade> students) {
        double examnote = -1;
        Student s = null;
        for (HashMap.Entry<Student, Grade> entry : students.entrySet()) {
            if (examnote < entry.getValue().getTotal())
                s = entry.getKey();
        }
        return s;
    }
}
