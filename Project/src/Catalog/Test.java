package Tema;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Test {
    public static void main(String args[]) {

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("/Users/karlaniculae/IdeaProjects/Tema/src/Tema/Tester1.json"));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray courses = (JSONArray) jsonObject.get("courses");
            Catalog catalog = Catalog.getInstance();
            for (int i = 0; i < courses.size(); i++) {
                JSONObject course = (JSONObject) courses.get(i);
                Course curs = null;
                String strategy = (String) course.get("strategy");
                Strategy strategy1 = null;
                if (strategy.equals("BestExamScore")) {
                    strategy1 = new BestExamScore();
                } else if (strategy.equals("BestPartialScore")) {
                    strategy1 = new BestPartialScore();
                } else if (strategy.equals("BestTotalScore")) {
                    strategy1 = new BestTotalScore();
                }
                String name = (String) course.get("name");

                JSONObject teacher = (JSONObject) course.get("teacher");
                String firstName = (String) teacher.get("firstName");
                String lastName = (String) teacher.get("lastName");
                Teacher teacher1 = (Teacher) UserFactory.getUser(UserFactory.UserType.Teacher, lastName, firstName);


                JSONArray assistants = (JSONArray) course.get("assistants");
                HashSet<Assistant> asistenti = new HashSet<Assistant>();
                if (assistants != null && !assistants.isEmpty()) {
                    for (int j = 0; j < assistants.size(); j++) {
                        JSONObject assistant = (JSONObject) assistants.get(j);
                        if (assistant != null) {

                            String nume = (String) assistant.get("firstName");
                            String prenume = (String) assistant.get("lastName");
                            Assistant a = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, nume, prenume);
                            asistenti.add(a);
                        }
                    }
                }


                if (course.get("type").equals("FullCourse")) {
                    curs = new FullCourse.FullCourseBuilder(name).asistenti(asistenti).profesor(teacher1).strategy(strategy1).build();
                } else {
                    curs = new PartialCourse.PartialCourseBuilder(name).asistenti(asistenti).profesor(teacher1).strategy(strategy1).build();
                }
                JSONArray groupsArray = (JSONArray) course.get("groups");
                for (Object groupObject : groupsArray) {
                    JSONObject group = (JSONObject) groupObject;
                    String ID = (String) group.get("ID");
                    JSONObject assistant = (JSONObject) group.get("assistant");
                    String assistantFirstName = (String) assistant.get("firstName");
                    String assistantLastName = (String) assistant.get("lastName");
                    Assistant ass1 = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, assistantLastName, assistantFirstName);
                    curs.addAssistant(ID, ass1);
                    Group g = new Group(ID, ass1);
                    JSONArray studentsArray = (JSONArray) group.get("students");
                    for (Object studentObject : studentsArray) {
                        JSONObject student = (JSONObject) studentObject;
                        String studentFirstName = (String) student.get("firstName");
                        String studentLastName = (String) student.get("lastName");
                        Student s = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                        JSONObject mother = (JSONObject) student.get("mother");
                        Parent p1 = null;
                        Parent p2 = null;
                        if (mother != null) {
                            String motherFirstName = (String) mother.get("firstName");
                            String motherLastName = (String) mother.get("lastName");
                            p1 = (Parent) UserFactory.getUser(UserFactory.UserType.Parent, motherLastName, motherFirstName);
                            boolean isObserver = false;
                            for (Observer obs : catalog.getObs()) {
                                if (obs != null && obs instanceof Parent) {
                                    Parent parent = (Parent) obs;
                                    if (parent.getName().equals(p1.getName()) && parent.getLastName().equals(p1.getLastName())) {
                                        isObserver = true;
                                        break;
                                    }
                                }
                            }
                            if (!isObserver) {
                                catalog.addObserver(p1);
                            }
                        }
                        JSONObject father = (JSONObject) student.get("father");
                        if (father != null) {
                            String fatherFirstName = (String) father.get("firstName");
                            String fatherLastName = (String) father.get("lastName");
                            p2 = (Parent) UserFactory.getUser(UserFactory.UserType.Parent, fatherLastName, fatherFirstName);
                            boolean isObserver = false;
                            for (Observer obs : catalog.getObs()) {
                                if (obs != null && obs instanceof Parent) {
                                    Parent parent = (Parent) obs;
                                    if (parent.getName().equals(p2.getName()) && parent.getLastName().equals(p2.getLastName())) {
                                        isObserver = true;
                                        break;
                                    }
                                }
                            }
                            if (!isObserver) {
                                catalog.addObserver(p2);
                            }
                        }
                        s.setMother(p1);
                        s.setFather(p2);
                        g.add(s);

                    }
                    curs.addGroup(g);
                }
                catalog.addCourse(curs);
            }
            Grade g = null;
            JSONArray examScores = (JSONArray) jsonObject.get("examScores");
            ScoreVisitor visit = new ScoreVisitor();
            for (int j = 0; j < examScores.size(); j++) {
                JSONObject examScore = (JSONObject) examScores.get(j);
                Teacher teacher3 = null;
                Student s2 = null;
                JSONObject teacher = (JSONObject) examScore.get("teacher");

                if (teacher != null) {
                    String teacherFirstName = (String) teacher.get("firstName");
                    String teacherLastName = (String) teacher.get("lastName");
                    teacher3 = (Teacher) UserFactory.getUser(UserFactory.UserType.Teacher, teacherLastName, teacherFirstName);

                }
                JSONObject student = (JSONObject) examScore.get("student");
                if (student != null) {
                    String studentFirstName = (String) student.get("firstName");
                    String studentLastName = (String) student.get("lastName");
                    s2 = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                }
                String course = (String) examScore.get("course");
                double grade = (double) examScore.get("grade");
                for (Course coursess : catalog.getCourses()) {
                    if (coursess.getName().equals(course)) {
                        for (Student std : coursess.getAllStudents()) {
                            if (s2.getLastName().equals(std.getLastName()) && s2.getName().equals(std.getName())) {
                                visit.addMap(teacher3, std, course, grade);
                            }
                        }
                    }

                }
                teacher3.accept(visit);
            }
            JSONArray partialScores = (JSONArray) jsonObject.get("partialScores");
            for (int j = 0; j < partialScores.size(); j++) {
                JSONObject partialScore = (JSONObject) partialScores.get(j);
                Assistant assistant3 = null;
                Student s2 = null;
                JSONObject assistant = (JSONObject) partialScore.get("assistant");
                if (assistant != null) {
                    String assistantFirstName = (String) assistant.get("firstName");
                    String assistantLastName = (String) assistant.get("lastName");
                    assistant3 = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, assistantLastName, assistantFirstName);
                }
                JSONObject student = (JSONObject) partialScore.get("student");
                if (student != null) {
                    String studentFirstName = (String) student.get("firstName");
                    String studentLastName = (String) student.get("lastName");
                    s2 = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                }
                String course = (String) partialScore.get("course");
                double grade = (double) partialScore.get("grade");
                for (Course coursess : catalog.getCourses()) {
                    if (coursess.getName().equals(course)) {
                        for (Student std : coursess.getAllStudents()) {
                            if (s2.getLastName().equals(std.getLastName()) && s2.getName().equals(std.getName())) {
                                visit.addMap2(assistant3, std, course, grade);
                            }
                        }
                    }

                }
                assistant3.accept(visit);
            }
            for (Course crs : catalog.getCourses()) {
                System.out.println("CURS:" + crs.getName());
                System.out.println("PROFESOR:" + crs.getProfesor());
                System.out.println("Asistentii cursului:");
                for (Assistant ass : crs.getAsistenti())
                    System.out.println(ass);
                System.out.println("Studentii:");
                for (Student st : crs.getAllStudents())
                    System.out.println(st);
                System.out.println("Studentii absolventi:");
                for (Student std : crs.getGraduatedStudents()) {
                    System.out.println(std);
                }
                System.out.println("Notele tuturor studentilor:");
                for (Map.Entry<Student, Grade> entry : crs.gettAllStudentGrades().entrySet()) {
                    Student key = entry.getKey();
                    Grade value = entry.getValue();
                    System.out.println(key + ":" + value.getPartialScore() + " " + value.getExamScore() + " " + value.getTotal());
                }
                HashMap<String, Group> grp = crs.getGrupe();
                System.out.println("GRUPELE CURSULUI:");
                for (Map.Entry<String, Group> entry : grp.entrySet()) {
                    String key = entry.getKey();
                    Group value = entry.getValue();
                    System.out.println("\n");
                    System.out.println(value);
                    for (Student st : value.getStudents()) {
                        System.out.println("Student:" + st);
                        if (st.getFather() != null && st.getMother() != null)
                            System.out.println("Parintii studentului:" + st.getFather() + " " + st.getMother());
                        else if (st.getFather() != null) {
                            System.out.println("Tatal  studentului:" + st.getFather());
                        } else if (st.getMother() != null) {
                            System.out.println("Tatal  studentului:" + st.getMother());
                        } else {
                        }

                    }
                }
                System.out.println("STRATEGY:" + crs.getBestStudent());
                System.out.println("\n");
            }
            // TESTARE MEMENTO
            Course c = catalog.getCourses().get(0);
            System.out.println("Note initiale:");
            for (Grade gr : c.getGrades())
                System.out.println(gr.getStudent() + ":" + gr.getTotal());
            c.makeBackup();
            Student nou = new Student("Niculae", "Karla");
            c.getGrupe().get("314CC").add(nou);
            visit.addMap2(c.getGrupe().get("314CC").getAssistent(), nou, "Programare Orientata pe Obiecte", 3.5);
            visit.addMap(c.getProfesor(), nou, "Programare Orientata pe Obiecte", 3.5);
            c.getGrupe().get("314CC").getAssistent().accept(visit);
            c.getProfesor().accept(visit);
            System.out.println("Note dupa adaugare student:");
            for (Grade gr : c.getGrades())
                System.out.println(gr.getStudent() + ":" + gr.getTotal());
            c.undo();
            System.out.println("Note undo:");
            for (Grade gr : c.getGrades())
                System.out.println(gr.getStudent() + ":" + gr.getPartialScore());

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
