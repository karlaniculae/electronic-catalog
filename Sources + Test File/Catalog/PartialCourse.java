package Tema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class PartialCourse extends Course {
    public PartialCourse(CourseBuilder builder) {
        super(builder);
    }

    public ArrayList<Student> getGraduatedStudents() {
        ArrayList<Student> list = new ArrayList<Student>();
        double nota = 0;
        for (int i = 0; i < getAllStudents().size(); i++) {
            Student s = getAllStudents().get(i);
            HashMap<Student, Grade> studentGrades = gettAllStudentGrades();
            if (studentGrades.containsKey(s)) {
                nota = studentGrades.get(s).getTotal();
            }
            if (nota > 5) {
                list.add(s);
            }
        }
        return list;
    }

    static class PartialCourseBuilder extends CourseBuilder {
        public PartialCourseBuilder(String name) {
            super(name);
        }

        public Course build() {
            return new PartialCourse(this);
        }
    }
}
