package Tema;

import java.util.Objects;

public class Assistant extends User implements Element {
    public Assistant(String lastName, String firstName) {
        super(lastName, firstName);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Assistant assistant = (Assistant) o;
        return Objects.equals(this.getLastName(), assistant.getLastName()) &&
                Objects.equals(this.getName(), assistant.getName());
    }

    public int hashCode() {
        return Objects.hash(this.getLastName(), this.getName());
    }
}
