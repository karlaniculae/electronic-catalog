package Tema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Catalog implements Subject {
    HashMap<Parent, Notification> hash = new HashMap<>();
    private List<Observer> obs = new ArrayList<>();
    private List<Course> list = new ArrayList<Course>();
    private static Catalog instance = null;

    private Catalog() {
    }

    public static Catalog getInstance() {
        if (instance == null) {
            instance = new Catalog();
        }
        return instance;
    }

    public void addCourse(Course course) {
        boolean found = false;
        for (Course c : list) {
            if (c.getName().equals(course.getName())) {
                found = true;
                break;
            }
        }
        if (!found) {
            list.add(course);
        }
    }

    public List<Course> getCourses() {
        return list;
    }

    public void removeCourse(Course course) {
        if (list.contains(course)) {
            list.remove(course);
        }
    }


    public void addObserver(Observer observer) {
        if (!obs.contains(observer)) {
            obs.add(observer);
        }
    }

    public void removeObserver(Observer observer) {
        int index = obs.indexOf(observer);
        System.out.println("Observer" + (index + 1) + "deleted");
        obs.remove(index);
    }

    public void notifyObservers(Grade grade) {
        Student student = grade.getStudent();
        for (Observer observer : obs) {
            if (observer != null && observer instanceof Parent) {
                Parent parent = (Parent) observer;
                if (student.getMother() != null && parent.getName().equals(student.getMother().getName()) && parent.getLastName().equals(student.getMother().getLastName())) {
                    parent.update(new Notification(grade));
                    hash.put(student.getMother(), new Notification(grade));
                }
                if (student.getFather() != null && parent.getName().equals(student.getFather().getName()) && parent.getLastName().equals(student.getFather().getLastName())) {
                    parent.update(new Notification(grade));
                    hash.put(student.getFather(), new Notification(grade));
                }
            }
        }
    }

    public List<Observer> getObs() {
        return obs;
    }
}
