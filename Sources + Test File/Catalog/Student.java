package Tema;

public class Student extends User implements Comparable<Student> {
    private Parent mother, father;

    public Student(String lastName, String firstName) {
        super(lastName, firstName);
    }

    public void setMother(Parent mother) {
        this.mother = mother;
    }

    public void setFather(Parent father) {
        this.father = father;
    }

    public Parent getMother() {
        return mother;
    }

    public Parent getFather() {
        return father;
    }

    public int compareTo(Student other) {
        int lastNameCompare = this.getLastName().compareTo(other.getLastName());
        if (lastNameCompare == 0) {
            return this.getName().compareTo(other.getName());
        } else {
            return lastNameCompare;
        }
    }
}
