package Tema;

import java.util.Objects;

public class Teacher extends User implements Element {
    public Teacher(String lastName, String firstName) {
        super(lastName, firstName);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(this.getLastName(), teacher.getLastName()) &&
                Objects.equals(this.getName(), teacher.getName());
    }

    public int hashCode() {
        return Objects.hash(this.getLastName(), this.getName());
    }
}