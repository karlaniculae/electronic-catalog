package Tema;

import com.sun.scenario.effect.impl.sw.java.JSWBlend_GREENPeer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.List;


public class SwingInterface extends JFrame implements ActionListener, ListSelectionListener {
    JButton button;
    JList<String> list;
    DefaultListModel<String> model;
    JLabel label1, label2;
    JTextArea information;
    JTextField searchStudent;
    JScrollPane first;

    public SwingInterface() {
        super("Catalog");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel cardPanel = new JPanel(new CardLayout());
        JPanel first1 = new JPanel();
        JPanel first2 = new JPanel();
        first1.setLayout(new GridLayout(2, 2));
        first1.setLayout(new GridLayout(2, 1));
        label1 = new JLabel("Student search:");
        label1.setForeground(Color.orange);
        information = new JTextArea();
        information.setEditable(false);
        searchStudent = new JTextField();
        button = new JButton("Search");
        button.addActionListener(this);
        button.setForeground(Color.CYAN);
        label2 = new JLabel("UPB ACS");
        label2.setForeground(Color.CYAN);
        list = new JList<>();
        list.addListSelectionListener(this);
        first = new JScrollPane(list);
        first.setVisible(true);
        model = new DefaultListModel<String>();
        list.setModel(model);
        first1.add(label1);
        first1.add(searchStudent);
        first1.add(label2);
        first1.add(button);
        add(first1, BorderLayout.NORTH);
        add(information, BorderLayout.CENTER);
        add(first, BorderLayout.SOUTH);
        pack();
        setSize(500, 500);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String firstname = null;
        String lastName = null;
        if (e.getSource() instanceof JButton) {
            model.clear();
            String[] s = searchStudent.getText().split(" ");
            int ok = 0;
            if (s.length > 1) {
                firstname = s[1];
                lastName = s[0];
                Catalog c = Catalog.getInstance();
                for (Course courses : c.getCourses()) {
                    for (Student std : courses.getAllStudents())
                        if (std.getLastName().equals(lastName) && std.getName().equals(firstname)) {
                            model.addElement(courses.getName());
                            ok = 1;
                        }
                }
            } else {
                JOptionPane.showMessageDialog(null, "You didn't write the complete name!", "Warning", JOptionPane.WARNING_MESSAGE);
                searchStudent.setText("");
            }
            if (ok == 0) {
                JOptionPane.showMessageDialog(null, "The student does not exists!", "Warning", JOptionPane.WARNING_MESSAGE);
                searchStudent.setText("");
            }
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        int index;
        if (list.isSelectionEmpty())
            return;
        else {
            information.setText("");
            Course b = null;
            index = list.getSelectedIndex();
            String str = model.get(index);
            Catalog catalog = Catalog.getInstance();
            for (Course curs : catalog.getCourses()) {
                if (curs.getName().equals(str))
                    b = curs;
            }
            information.setBorder(BorderFactory.createTitledBorder("Information:" + b.getName()));
            information.append("The titular teacher of the course:" + b.getProfesor().getLastName() + " " + b.getProfesor().getName() + "\n");
            information.append("Course assistants:" + "\n");
            for (Assistant a : b.getAsistenti()) {
                information.append(a.getLastName() + " " + a.getName() + "\n");
            }
            String[] s = searchStudent.getText().split(" ");
            String firstname = s[1];
            String lastName = s[0];
            information.append("Student assistant:");
            for (Group grp : b.getGrupe().values()) {
                for (Student stdt : grp.getStudents()) {
                    if (stdt.getLastName().equals(lastName) && stdt.getName().equals(firstname)) {
                        information.append(grp.getAssistent().getLastName() + " " + grp.getAssistent().getName() + "\n");
                    }

                }

            }
            information.append(b.getName() + "-student grades:" + "\n");
            for (Map.Entry<Student, Grade> entry : b.gettAllStudentGrades().entrySet()) {
                if (entry.getKey().getName().equals(firstname) && entry.getKey().getLastName().equals(lastName)) {
                    information.append("PartialScore:" + entry.getValue().getPartialScore() + "\n");
                    information.append("ExamScore:" + entry.getValue().getExamScore() + "\n");
                    information.append("TotalScore:" + entry.getValue().getTotal() + "\n");
                }
            }
        }
    }

    public static void main(String args[]) {
        SwingInterface swing = new SwingInterface();

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("/Users/karlaniculae/IdeaProjects/Tema/src/Tema/Tester1.json"));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray courses = (JSONArray) jsonObject.get("courses");
            Catalog catalog = Catalog.getInstance();
            List<Course> cursuri = new ArrayList<>();
            for (int i = 0; i < courses.size(); i++) {
                JSONObject course = (JSONObject) courses.get(i);
                Course curs = null;


                String strategy = (String) course.get("strategy");
                Strategy strategy1 = null;
                if (strategy.equals("BestExamScore")) {
                    strategy1 = new BestExamScore();
                } else if (strategy.equals("BestPartialScore")) {
                    strategy1 = new BestPartialScore();
                } else if (strategy.equals("BestTotalScore")) {
                    strategy1 = new BestTotalScore();
                }
                String name = (String) course.get("name");

                JSONObject teacher = (JSONObject) course.get("teacher");
                String firstName = (String) teacher.get("firstName");
                String lastName = (String) teacher.get("lastName");
                Teacher teacher1 = (Teacher) UserFactory.getUser(UserFactory.UserType.Teacher, lastName, firstName);


                JSONArray assistants = (JSONArray) course.get("assistants");
                HashSet<Assistant> asistenti = new HashSet<Assistant>();
                if (assistants != null && !assistants.isEmpty()) {
                    for (int j = 0; j < assistants.size(); j++) {
                        JSONObject assistant = (JSONObject) assistants.get(j);
                        if (assistant != null) {

                            String nume = (String) assistant.get("firstName");
                            String prenume = (String) assistant.get("lastName");
                            Assistant a = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, nume, prenume);
                            asistenti.add(a);
                        }
                    }
                }
                if (course.get("type").equals("FullCourse")) {
                    curs = new FullCourse.FullCourseBuilder(name).asistenti(asistenti).profesor(teacher1).strategy(strategy1).build();
                } else {
                    curs = new PartialCourse.PartialCourseBuilder(name).asistenti(asistenti).profesor(teacher1).strategy(strategy1).build();
                }
                JSONArray groupsArray = (JSONArray) course.get("groups");
                for (Object groupObject : groupsArray) {
                    JSONObject group = (JSONObject) groupObject;
                    String ID = (String) group.get("ID");
                    JSONObject assistant = (JSONObject) group.get("assistant");
                    String assistantFirstName = (String) assistant.get("firstName");
                    String assistantLastName = (String) assistant.get("lastName");
                    Assistant ass1 = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, assistantLastName, assistantFirstName);
                    curs.addAssistant(ID, ass1);
                    Group g = new Group(ID, ass1);
                    JSONArray studentsArray = (JSONArray) group.get("students");
                    for (Object studentObject : studentsArray) {
                        JSONObject student = (JSONObject) studentObject;
                        String studentFirstName = (String) student.get("firstName");
                        String studentLastName = (String) student.get("lastName");
                        Student s = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                        JSONObject mother = (JSONObject) student.get("mother");
                        Parent p1 = null;
                        Parent p2 = null;
                        if (mother != null) {
                            String motherFirstName = (String) mother.get("firstName");
                            String motherLastName = (String) mother.get("lastName");
                            p1 = (Parent) UserFactory.getUser(UserFactory.UserType.Parent, motherLastName, motherFirstName);
                            boolean isObserver = false;
                            for (Observer obs : catalog.getObs()) {
                                if (obs != null && obs instanceof Parent) {
                                    Parent parent = (Parent) obs;
                                    if (parent.getName().equals(p1.getName()) && parent.getLastName().equals(p1.getLastName())) {
                                        isObserver = true;
                                        break;
                                    }
                                }
                            }
                            if (!isObserver) {
                                catalog.addObserver(p1);
                            }
                        }
                        JSONObject father = (JSONObject) student.get("father");
                        if (father != null) {
                            String fatherFirstName = (String) father.get("firstName");
                            String fatherLastName = (String) father.get("lastName");
                            p2 = (Parent) UserFactory.getUser(UserFactory.UserType.Parent, fatherLastName, fatherFirstName);
                            boolean isObserver = false;
                            for (Observer obs : catalog.getObs()) {
                                if (obs != null && obs instanceof Parent) {
                                    Parent parent = (Parent) obs;
                                    if (parent.getName().equals(p2.getName()) && parent.getLastName().equals(p2.getLastName())) {
                                        isObserver = true;
                                        break;
                                    }
                                }
                            }
                            if (!isObserver) {
                                catalog.addObserver(p2);
                            }
                        }
                        s.setMother(p1);
                        s.setFather(p2);
                        g.add(s);
                    }
                    curs.addGroup(g);
                }
                catalog.addCourse(curs);
            }
            Grade g = null;
            JSONArray examScores = (JSONArray) jsonObject.get("examScores");
            ScoreVisitor visit = new ScoreVisitor();
            for (int j = 0; j < examScores.size(); j++) {
                JSONObject examScore = (JSONObject) examScores.get(j);
                Teacher teacher3 = null;
                Student s2 = null;
                JSONObject teacher = (JSONObject) examScore.get("teacher");

                if (teacher != null) {
                    String teacherFirstName = (String) teacher.get("firstName");
                    String teacherLastName = (String) teacher.get("lastName");
                    teacher3 = (Teacher) UserFactory.getUser(UserFactory.UserType.Teacher, teacherLastName, teacherFirstName);

                }
                JSONObject student = (JSONObject) examScore.get("student");
                if (student != null) {
                    String studentFirstName = (String) student.get("firstName");
                    String studentLastName = (String) student.get("lastName");
                    s2 = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                }
                String course = (String) examScore.get("course");
                double grade = (double) examScore.get("grade");
                for (Course coursess : catalog.getCourses()) {
                    if (coursess.getName().equals(course)) {
                        for (Student std : coursess.getAllStudents()) {
                            if (s2.getLastName().equals(std.getLastName()) && s2.getName().equals(std.getName())) {
                                visit.addMap(teacher3, std, course, grade);
                            }
                        }
                    }

                }
                teacher3.accept(visit);
            }
            JSONArray partialScores = (JSONArray) jsonObject.get("partialScores");
            for (int j = 0; j < partialScores.size(); j++) {
                JSONObject partialScore = (JSONObject) partialScores.get(j);
                Assistant assistant3 = null;
                Student s2 = null;
                JSONObject assistant = (JSONObject) partialScore.get("assistant");
                if (assistant != null) {
                    String assistantFirstName = (String) assistant.get("firstName");
                    String assistantLastName = (String) assistant.get("lastName");
                    assistant3 = (Assistant) UserFactory.getUser(UserFactory.UserType.Assistant, assistantLastName, assistantFirstName);
                }
                JSONObject student = (JSONObject) partialScore.get("student");
                if (student != null) {
                    String studentFirstName = (String) student.get("firstName");
                    String studentLastName = (String) student.get("lastName");
                    s2 = (Student) UserFactory.getUser(UserFactory.UserType.Student, studentLastName, studentFirstName);
                }
                String course = (String) partialScore.get("course");
                double grade = (double) partialScore.get("grade");
                for (Course coursess : catalog.getCourses()) {
                    if (coursess.getName().equals(course)) {
                        for (Student std : coursess.getAllStudents()) {
                            if (s2.getLastName().equals(std.getLastName()) && s2.getName().equals(std.getName())) {
                                visit.addMap2(assistant3, std, course, grade);
                            }
                        }
                    }

                }
                assistant3.accept(visit);
            }


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
